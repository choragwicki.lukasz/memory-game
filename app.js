const start = document.querySelector('.start');
const score = document.querySelector('.score');
const easy = document.querySelector('.easy');
const hard = document.querySelector('.hard');
const containerLvl = document.querySelector('.level-container');
const time = document.querySelector('.time');
const tilesImg = [
    "images/1.jpg",
    "images/2.jpg",
    "images/3.jpg",
    "images/4.jpg",
    "images/5.jpg",
    "images/6.jpg",
    "images/7.jpg",
    "images/8.jpg",
    "images/9.jpg",
    "images/10.jpg",
];

let tileCount = 16;
let tileOnRow = 4;
let divBoard = null;
let divScore = null;
let tiles = [];
let tilesChecked = [];
let moveCount = 0;
let canGet = true;
let min = null;
let sec = null;
let hitCards = 0;
let setTimeoutGame = null;

const selectLvl = () => {
  hard.addEventListener('click', () => {
      tileCount = 20;
      tileOnRow = 5;
      divBoard.style.gridTemplateColumns = 'repeat(5, 1fr)';
      containerLvl.classList.remove('level-container--active');
      min = 0;
      sec = 5;
      startGames();
      getTime();
  });

  easy.addEventListener('click', () => {
      tileCount = 16;
      tileOnRow = 4;
      divBoard.style.gridTemplateColumns = "repeat(4, 1fr)";
      containerLvl.classList.remove('level-container--active');
      min = 1;
      sec = 10;
      startGames();
      getTime();
  });
};

selectLvl();

const startGames = () => {
    divBoard = document.querySelector('.board');
    divScore = document.querySelector('.score');
    divBoard.innerHTML = '';
    time.innerHTML = '';
    divScore.innerText = 'Trials: ' + 0;
    tiles = [];
    tilesChecked = [];
    moveCount = 0;
    canGet = true;

    for (let i = 0; i < tileCount; i++) {
        tiles.push(Math.floor(i / 2));
    }

    for (let i = tileCount - 1 ; i > 0; i--) {
        const swap = Math.floor(Math.random() * i);
        const tmp = tiles[i];
        tiles[i] = tiles[swap];
        tiles[swap] = tmp;
    }

    for (let i = 0; i < tileCount; i++) {
        const tile = document.createElement('div');
        tile.classList.add('card');
        divBoard.appendChild(tile);

        tile.dataset.cardType = tiles[i];
        tile.dataset.index = i;
        tile.addEventListener('click', cardClick);
    }
};

const clickStart = () => start.addEventListener('click', () => {
    containerLvl.classList.add('level-container--active');
    Array.from(document.querySelectorAll('.card')).forEach(el => el.classList.remove('no-active'));
    document.querySelector('.board').classList.remove('game-over', 'winner');
});

const deleteCarts = () => {
    tilesChecked.forEach(el => {
        const emptyDiv = document.createElement('div');
        el.after(emptyDiv);
        el.remove();
    });

    canGet = true;
    tilesChecked = [];
    hitCards++;
    if (hitCards >= tileCount / 2) {
        clearTimeout(setTimeoutGame);
        Array.from(document.querySelectorAll('.card')).forEach(el => el.classList.add('no-active'));
        document.querySelector('.board').classList.add('winner');
    }
};

const resetCarts = () => {
    tilesChecked.forEach(el => el.style.backgroundImage = '');
    tilesChecked = [];
    canGet = true;
};

const cardClick = (e) => {
    if (canGet) {
        if (!tilesChecked[0] || (tilesChecked[0].dataset.index !== e.target.dataset.index)) {
            tilesChecked.push(e.target);
            e.target.style.backgroundImage = `url(${tilesImg[e.target.dataset.cardType]})`;
            moveCount++;
            score.textContent = `Trials: ${Math.floor(moveCount / 2)}`;
        }
        if (tilesChecked.length === 2) {
            if (tilesChecked[0].dataset.cardType === tilesChecked[1].dataset.cardType) {
                setTimeout(deleteCarts, 100);
            } else {
                setTimeout(resetCarts, 500);
            }
        }
    }
};

const getTime = () => {
    sec--;

    if (sec === 0 ) {
        if (min > 0) {
            min = min - 1;
            sec = 60;
        }
    }

    time.textContent = `Time: ${ min } :  ${ sec < 10 ? '0' + sec : sec } `;
    setTimeoutGame = setTimeout(getTime, 1000);

    if (sec === 0 && min === 0 ) {
        clearTimeout(setTimeoutGame);
        Array.from(document.querySelectorAll('.card')).forEach(el => el.classList.add('no-active'));
        document.querySelector('.board').classList.add('game-over');
    }
};

clickStart();
startGames();


